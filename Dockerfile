# stage 0: select the base image;
FROM docker.io/c4f3z1n/s6-alpine as base

# stage 1: download and verify the binaries;
FROM base as download

ARG ARCH
ARG NEBULA_VERSION

RUN apk add --no-cache curl jq
RUN execlineb -P -c ' \
	### BEGIN (execline code block);
	backtick -E CWD { mktemp -u } \
	if { mkdir -pv "${CWD}/bin" } \
	execline-cd ${CWD} \
	define releases_api_url "https://api.github.com/repos/slackhq/nebula/releases" \
	# if NEBULA_VERSION is not set, retrieve the latest version from GitHub;
	backtick -E NEBULA_VERSION { \
		if -nt { printenv NEBULA_VERSION } \
		pipeline { curl -fsSL "${releases_api_url}/latest" } \
		jq -cr ".tag_name" \
	} \
	# if ARCH is not set, detect and convert the string to the expected naming convention;
	backtick -E ARCH { \
		if -nt { printenv ARCH } \
		backtick -E arch { apk --print-arch } \
		ifelse { heredoc 0 ${arch} grep -q -e "aarch64*" -e "arm*8*" } { echo "arm64" } \
		ifelse { heredoc 0 ${arch} grep -q "arm" } { \
			backtick -E version { heredoc 0 ${arch} tr -cd "0-9" } \
			echo "arm-${version}" \
		} \
		ifelse { heredoc 0 ${arch} grep -q "x86_64" } { echo "amd64" } \
		ifelse { heredoc 0 ${arch} grep -q "x86" } { echo "386" } \
		echo ${arch} \
	} \
	# retrieve and filter content from the API;
	backtick -E filtered_json { \
		pipeline { curl -fsSL ${releases_api_url} } \
		jq -cr "[ \
			.[] | \
			select(.tag_name == \"${NEBULA_VERSION}\") | \
			.assets[] | \
			select(.name | test(\"linux-${ARCH}.tar.gz|SHASUM256.txt\")) | \
			{ name: .name, url: .browser_download_url } \
		]" \
	} \
	# download and extract the binaries;
	backtick -E tarball { \
		heredoc 0 ${filtered_json} \
		jq -cr ".[] | select(.name | contains(\".tar.gz\")) | .name" \
	} \
	if { \
		pipeline { \
			heredoc 0 ${filtered_json} \
			jq -cr ".[] | select(.name == \"${tarball}\") | .url" \
		} \
		xargs curl -fL -o ${tarball} \
	} \
	if { tar -xzf ${tarball} -C bin } \
	if { \
		# verify SHA256 hashes;
		pipeline { \
			heredoc 0 ${filtered_json} \
			jq -cr ".[] | select(.name == \"SHASUM256.txt\") | .url" \
		} \
		pipeline { xargs curl -fsSL } \
		pipeline { grep ${tarball} } \
		pipeline { sed "s,${tarball}/,bin/," } \
		sha256sum -c \
	} \
	# make the binaries executable and available for the next stage;
	if { elglob i "bin/*" chmod -v a+x ${i} } \
	ln -fsv ${CWD} "/tmp/downloads" \
	# ### END (execline code block);
	'

# stage 2: final adjustments;
FROM base

RUN apk add --no-cache \
		inotify-tools \
		libcap \
		procps \
		yq

COPY --from=download "/tmp/downloads/bin" "/usr/sbin"
COPY overlay-rootfs /

ARG CONFIG_DIR="/config"
ENV CONFIG_DIR=${CONFIG_DIR}
VOLUME ${CONFIG_DIR}
WORKDIR ${CONFIG_DIR}
